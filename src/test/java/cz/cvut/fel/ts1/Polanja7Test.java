package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class Polanja7Test {
    @Test
    public void factorialTest() {
        Polanja7 test_instance = new Polanja7();
        long expected_result = 120;
        long result = test_instance.factorial(5);
        assertEquals(expected_result, result);
    }
}
